import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions, Chart } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {

  public ChartOptions: ChartOptions = {
    responsive: true,
  };
  public Orders : any = [] ;
  public amountList :  any =[];
  public volumeList :  any =[];
  public tickerList :  any =[];

  public ChartLabels: Label[] = this.tickerList ;
  public ChartData: SingleDataSet = this.amountList;
  public ChartData2: SingleDataSet = this.volumeList;
  public ChartType1: ChartType = 'pie';
  public ChartType2: ChartType = 'pie';
  public ChartPlugins = [];
  public  totalAmount : number = 0;
  public chartColors: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870', '#A8B3C5', '#616774'],
      borderWidth: 2,
    }
  ];
  
   
  constructor(public restApi: RestApiService) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }
  ngOnInit(): void {
    this.loadOrders();
  }
  loadOrders() {
    return this.restApi.getPortfolios().subscribe((data: {}) => {
          this.Orders = data;
          console.log(this.Orders);
          for (var i = 0; i < this.Orders.length; i++) {
              this.tickerList.push(this.Orders[i].stockTicker);
              this.volumeList.push(this.Orders[i].volume);
              this.amountList.push(this.Orders[i].amount);
              this.totalAmount = this.totalAmount + this.Orders[i].amount;
          }
          this.ChartLabels = this.tickerList
         

        }); 
    }

    piechart1(){
      this.ChartType1 = 'pie';
    }
    barchart1(){
      this.ChartType1 = 'bar';
    
    }
     piechart2(){
      this.ChartType2 = 'pie';
    }
    barchart2(){
      this.ChartType2 = 'bar';
    
    }
  }  

