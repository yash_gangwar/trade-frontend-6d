import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TradeListComponent } from './trade-list/trade-list.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { PlaceOrderComponent} from './place-order/place-order.component';
import { OrderStatusComponent } from './order-status/order-status.component';
import {OrderEditComponent} from './order-edit/order-edit.component';
import {PieChartComponent} from './pie-chart/pie-chart.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: 'watchlist', component: DashboardComponent},
  { path: 'trade-list', component: TradeListComponent } ,
  { path: 'trade-list', component: TradeListComponent }  ,
  { path: 'create-order', component: PlaceOrderComponent },
  {path: 'order-status', component: OrderStatusComponent},
  {path: 'order-edit/:id', component: OrderEditComponent},
  {path: 'home', component: PieChartComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [PlaceOrderComponent]
