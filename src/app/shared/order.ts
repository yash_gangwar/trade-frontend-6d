export class Order {

    constructor(){
        this.id = 0, 
        this.stockTicker ="", 
        this.price = 0, 
        this.volume = 0, 
        this.buyOrSell ="", 
        this.statusCode =""
    }
    
    id: number; 
    stockTicker: string;
    price: number; 
    volume: number; 
    buyOrSell: string; 
    statusCode: string;
}
