export class Portfolio {

    constructor() {
        this.id = 0, 
        this.stockTicker ="", 
        this.price = 0, 
        this.volume = 0
    }
    id: number; 
    stockTicker: string;
    price: number; 
    volume: number; 
}
