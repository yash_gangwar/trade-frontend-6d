export class Ticker {

    constructor(){
        this.symbol ="", 
        this.name = "", 
        this.country = "", 
        this.industry ="",
        this.stockPrice=0
    }
    
    symbol: string;
    name: string; 
    country: string; 
    industry: string; 
    stockPrice: number;
}
