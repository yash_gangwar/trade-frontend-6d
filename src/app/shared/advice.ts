export class Advice {
    constructor() {
        this.ticker = "", 
        this.lastClose =0, 
        this.upperBand = 0, 
        this.lowerBand = 0,
        this.advice = ""
    }
    lastClose: number; 
    ticker: string;
    upperBand: number; 
    lowerBand: number; 
    advice: string;
}

