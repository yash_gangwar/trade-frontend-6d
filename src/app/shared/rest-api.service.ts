import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Order } from '../shared/order';
import { Ticker } from '../shared/ticker';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Portfolio } from './portfolio';
import{Advice} from './advice';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  apiURL = 'http://localhost:8080/api/orders/';
  watchURL = 'http://localhost:8080/api/watch/';
  portfolioURL = 'http://localhost:8080/portfolio/';


  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
  }  

  getStockTickers(): Observable<string[]> {
    return this.http.get<string[]>(this.apiURL+'tickerList')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getStockPrice(tickerSymbol: string): Observable<any> {
    return this.http.get(this.watchURL + 'price' + '?symbol=' + tickerSymbol)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method 
  getOrders(): Observable<Order> {
    return this.http.get<Order>(this.apiURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getPortfolios(): Observable<Portfolio> {
    return this.http.get<Portfolio>(this.portfolioURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method 
  getOrder(id:any): Observable<Order> {
    return this.http.get<Order>(this.apiURL + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  getOrderStatus(tickerName: string): Observable<Order> {
    return this.http.get<Order>(this.apiURL + "filter?ticker=" + tickerName)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  getTradeAdvice(ticker: string):  Observable<Advice> {
    return this.http.get<Advice>("https://qz4sxjl623.execute-api.us-east-1.amazonaws.com/default/tradeAdvisor?ticker=" + ticker)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getTickerList() : Observable<Order> {
    return this.http.get<Order>(this.apiURL + "orderTickerList")
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API post() method 
  createOrder(Order:Order): Observable<Order> {
    return this.http.post<Order>(this.apiURL + '', JSON.stringify(Order), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } 

  // HttpClient API put() method
  updateOrder(id:number, Order:Order): Observable<Order> {
    return this.http.put<Order>(this.apiURL, JSON.stringify(Order), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method 
  deleteOrder(id:number){
    return this.http.delete<Order>(this.apiURL + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  
  // Error handling 
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
  
 showWatchlist(): Observable<Ticker> {
    return this.http.get<Ticker>(this.watchURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getTickers(): Observable<any> {
    return this.http.get<String>(this.apiURL+"tickerList/");
    }

  putTicker(ticker:String): Observable<Ticker> {
    return this.http.put<Ticker>(this.watchURL+'add?symbol='+ticker,{} , this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  deleteTicker(ticker:string): Observable<any> {
    return this.http.put(this.watchURL+'del?symbol='+ticker, {}, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
}

