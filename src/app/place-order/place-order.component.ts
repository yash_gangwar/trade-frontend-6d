import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService} from '../shared/rest-api.service';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.css']
})
export class PlaceOrderComponent implements OnInit {
 
  stockTickers: any= [];
  @Input() orderDetails = { id: 0, stockTicker: 'AAPL', price: 0, volume: 0, buyOrSell: '', statusCode: '' }

  constructor(
    public restApi: RestApiService, 
    public router: Router,
    public activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void { 
    this.getStockTickers();
    
  }

  getStockTickers(){
    return this.restApi.getStockTickers().subscribe((tickerList) => {
        this.stockTickers = tickerList;
        this.activatedRoute.paramMap.subscribe(()=> {
          if (window.history.state['stockTicker']) {
          this.orderDetails.stockTicker = window.history.state['stockTicker'];
        }
        else this.orderDetails.stockTicker = 'AAPL'
        }); 
        this.fillPrice();  
    })
  }

  addOrder() {
    console.log(this.orderDetails);
    this.restApi.createOrder(this.orderDetails).subscribe((data: {}) => {
      this.router.navigate(['/trade-list'])
    })
  } 

  fillPrice() {
    this.restApi.getStockPrice(this.orderDetails.stockTicker).subscribe((tickerPrice) => {
      this.orderDetails.price = tickerPrice["price"];
      console.log(this.orderDetails.price);  
    })
  } 
}
