import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { HttpClientModule } from "@angular/common/http"

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  Watch: any = [];
  optionList: any = ["op1","op2","op3","op4"];

  selectedTicker:string="";
  constructor(public restApi: RestApiService, private httpClient: HttpClientModule) { }

  ngOnInit(): void {
    this.loadWatchlist();
    this.restApi.getTickers().subscribe((data: {}) => {
      this.optionList = data;      
    });   
  }

  addTicker() {
    console.log(this.selectedTicker);
    this.restApi.putTicker(this.selectedTicker).subscribe((data: {}) => {
        console.log(data);
        this.loadWatchlist();
    })
  }

  loadWatchlist() {
    return this.restApi.showWatchlist().subscribe((data: {}) => {
      this.Watch = data;
    })
  }

  deleteTicker(id:string) {
    return this.restApi.deleteTicker(id).subscribe(() => {
        this.loadWatchlist()
      });
  }

}
