import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-trade-list',
  templateUrl: './trade-list.component.html',
  styleUrls: ['./trade-list.component.css']
})
export class TradeListComponent implements OnInit {

  active = 1;
  Orders: any = [];
  processing: number = 0;
  executed: number = 0;
  constructor(public restApi: RestApiService)
   {

   }

   ngOnInit(): void {
   
      this.loadOrders();
  
  }


  loadOrders() {
    return this.restApi.getOrders().subscribe((data: {}) => {
        this.Orders = data;
        this.Orders.forEach((element: { statusCode: number; }) => {
          if (element.statusCode < 2)
            this.processing ++;
          else 
            this.executed ++;
        });
    })
  }  

  deleteOrder(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteOrder(id).subscribe(data => {
        this.loadOrders()
      })
    }
  }  

}

