import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import { FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-order-status',
  templateUrl: './order-status.component.html',
  styleUrls: ['./order-status.component.css']
})
export class OrderStatusComponent implements OnInit {

  tickerName: any = [];
  orderStatus: any = [];
  public verbose: boolean = true;

  constructor(public fb: FormBuilder, public rest: RestApiService) { }

  ngOnInit(): void {
    this.loadDropdown();
  }

  tickerNameForm = this.fb.group({
    name: ['']
  })

  loadDropdown() {
    return this.rest.getTickerList().subscribe((data: {}) => {
      this.tickerName = data;
      console.log(this.tickerName);
  })

  }
  
  onSubmit() {
    this.rest.getOrderStatus(this.tickerNameForm.value.name).subscribe((data: {}) => {
          this.orderStatus = data;
          console.log(this.orderStatus);
          this.verbose = false;

      })
    
  }

}