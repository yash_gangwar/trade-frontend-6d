import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Advice } from '../shared/advice';
import { RestApiService} from '../shared/rest-api.service';

@Component({
  selector: 'app-trade-advisor',
  templateUrl: './trade-advisor.component.html',
  styleUrls: ['./trade-advisor.component.css']
})
export class TradeAdvisorComponent implements OnInit {
  stockTickers: any= [];
  @Input() stockTicker: string = "AAPL";
  data: any = [];
  public verbose = true;

  constructor(
    public restApi: RestApiService, 
    public router: Router,
    public activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void { 
    this.getStockTickers();
    
  }
  getStockTickers(){
    return this.restApi.getStockTickers().subscribe((tickerList) => {
        this.stockTickers = tickerList;
        this.activatedRoute.paramMap.subscribe(()=> {
          if (window.history.state['stockTicker']) {
          this.stockTicker = window.history.state['stockTicker'];
        }
        else this.stockTicker = 'AAPL'
        }); 
    })
  }

  tradeAdvice() {
    console.log(this.stockTicker);
    this.restApi.getTradeAdvice(this.stockTicker).subscribe((data: {}) => {
      this.data = data;
      console.log(this.data.advice);
      this.verbose = false;
    })
  } 



}
