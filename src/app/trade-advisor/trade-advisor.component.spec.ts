import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeAdvisorComponent } from './trade-advisor.component';

describe('TradeAdvisorComponent', () => {
  let component: TradeAdvisorComponent;
  let fixture: ComponentFixture<TradeAdvisorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradeAdvisorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeAdvisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
