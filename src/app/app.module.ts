import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ChartsModule } from'ng2-charts'; 

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { TradeListComponent } from './trade-list/trade-list.component';
import { PlaceOrderComponent } from './place-order/place-order.component';
import { OrderStatusComponent } from './order-status/order-status.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OrderEditComponent } from './order-edit/order-edit.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {PieChartComponent} from './pie-chart/pie-chart.component';
import { TradeAdvisorComponent } from './trade-advisor/trade-advisor.component';


@NgModule({
  declarations: [
    AppComponent,
    PlaceOrderComponent,
    routingComponents,
    TradeListComponent,
    OrderStatusComponent,
    OrderEditComponent,
    DashboardComponent,
    PieChartComponent,
    TradeAdvisorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
