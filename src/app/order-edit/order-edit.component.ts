import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-order-edit',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.css']
})
export class OrderEditComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  public orderDetails: any = {};
  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router,

  ) { }

  ngOnInit(): void {
    this.restApi.getOrder(this.id).subscribe((data: {}) => {
      this.orderDetails = data;
    })
  }

  updateOrder() {
    if(window.confirm('Are you sure, you want to update?')){
      this.restApi.updateOrder(this.id, this.orderDetails).subscribe(data => {
        this.router.navigate(['/trade-list'])
      })
    }
  }

}
